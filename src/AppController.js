
const api_url = 'http://localhost:8080/api';

export default class Controller {

    //ENVIAR USER AL LOCALSTORAGE (LOG IN)

    static saveUserLocal = (data) => {
        const json = JSON.stringify(data);
        localStorage.setItem('infoUser', json);
    }

    //OBTENER USER ID DEL LOCALSTORAGE

    static getLocalUserId = () => {
        const json = localStorage.getItem('infoUser');
        if (!json) return null;
        try {
            const user = JSON.parse(json);
            return user;
        } catch {
            return [];
        }
    }

    //ELIMINAR USER DEL LOCALSTORAGE (LOG OUT)

    static deleteUserId = (name) => {
        localStorage.removeItem('infoUser', name)
    }

    //SUBIR UNA NUEVA RECETA

    static addReceta = (item, userId) => {
        const nuevaReceta = JSON.stringify(item);
        const opcionesFetch = {
            method: "POST",
            body: nuevaReceta,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/" + userId + "/nueva", opcionesFetch)
            .then(resp => {
                console.log("nueva receta:", resp)
            })
            .catch(err => console.log("error nueva receta", err));

    }

    //CREAR NUEVO USUARIO

    static addUser = (item) => {
        const nuevaReceta = JSON.stringify(item);
        const opcionesFetch = {
            method: "POST",
            body: nuevaReceta,
            headers: { 'Content-Type': 'application/json' },
        }

        fetch(api_url + "/registro", opcionesFetch)
            .then(resp => {
                console.log("nuevo usuario:", resp)
            })
            .catch(err => console.log("error nuevo usuario", err));
    }

    //BUSCAR USUARIO
    static searchUser = (item) => {
        const promesa = (res, rej) => {
            fetch(api_url + "/usuarios")
            .then(data => data.json())
            .then(users => {
                const usuario = users.filter(el => el.nombre === item.nombre);
                res (usuario);
            })
            .catch(err => {
                rej(err);
            });
        };
        return new Promise(promesa);
    }

    //BUSCAR TODAS LAS RECETAS

    static getAllRecetas = () => {
        const promesa = (res, rej) => {
            fetch(api_url + '/recetas')
                .then(data => data.json())
                .then(receta => {
                    res(receta);
                })
                .catch(err => {
                    rej(err);
                });
        };
        return new Promise(promesa);
    }

    //BUSCAR RECETA POR ID

    static getRecebtaById = (recetaId) => {
        const promesa = (res, rej) => {
            fetch(api_url + '/receta/' + recetaId)
                .then(data => data.json())
                .then(receta => {
                    receta.id = receta._id;
                    res(receta);
                })
                .catch(err => {
                    rej(err);
                });
        };
        return new Promise(promesa);
    }
    
    //BUSCAR USUARIO POR ID

    static getUsuarioById = (usuarioId) => {
        const promesa = (res, rej) => {
            fetch(api_url + '/usuario/' + usuarioId)
                .then(data => data.json())
                .then(usuario => {
                    usuario.id = usuario._id;
                    res(usuario);
                })
                .catch(err => {
                    rej(err);
                });
        };
        return new Promise(promesa);
    }
}


