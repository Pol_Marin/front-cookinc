import React, { useState } from 'react';
import {
    Modal, ModalHeader, ModalBody, ModalFooter, 
    Button, Form, FormGroup, Label, Input 
} from 'reactstrap';

import Controller from './AppController'

const Registro = () => {

    const [nombre, setNombre] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');

    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);

    const usuario = {
        nombre,
        password,
        email
    }

    const registrado = () => {
        Controller.addUser(usuario);
        setModal(!modal)
    }

    return (
        <div>
            <div>
                <Button color="info" onClick={toggle} className="m-2 btn-sm">REGISTRARSE</Button>
                <Modal isOpen={modal} toggle={toggle}>
                    <ModalHeader toggle={toggle}>REGISTRATE</ModalHeader>
                    <ModalBody>
                        <Form>

                            <FormGroup >
                                <Label for="nombre">Nombre de usuario</Label>
                                <Input type="text" name="name" id="nombre" value={nombre} onChange={(e) => setNombre(e.target.value)}/>
                            </FormGroup>

                            <FormGroup >
                                <Label for="email">Email</Label>
                                <Input type="email" name="email" id="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
                            </FormGroup>

                            <FormGroup >
                                <Label for="password">Password</Label>
                                <Input type="password" name="password" id="password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                            </FormGroup>

                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={registrado}>Registrarse</Button>{' '}
                        <Button color="secondary" onClick={toggle}>Cancelar</Button>
                    </ModalFooter>
                </Modal>
            </div>
        </div>
    );
}

export default Registro;