import React, { useState, useEffect } from 'react';
import styled, { keyframes } from 'styled-components';
import { Link } from 'react-router-dom';
import {
    Card, Button, CardHeader, CardFooter, CardBody,
    CardTitle, CardText, CardImg, Row, Col, Input
} from 'reactstrap';
import Controller from './AppController';


const ImgPerfil = styled.img`
    border-radius: 100%;
    position: absolute;
    top: -8px;
    left: 18px;
    height: 42px;
    width: 42px;
`;

const beat = keyframes`
    from {
        height: 55px;
        width: 55px;
    }
    to{
        height: 45px;
        width: 45px;
    }
`;

const IconsRecetas = styled.img`
    height: 45px;
    cursor: pointer;
    margin-right: 15px;
    `;
//animation: ${beat} 0.3s 1;



const CardReceta = (props) => {

    const [yummy, setYummy] = useState("https://www.flaticon.com/svg/static/icons/svg/1933/1933811.svg");
    const [yummyBeat, setYummyBeat] = useState(0);
    const [favMenu, setFavMenu] = useState("https://www.flaticon.com/svg/static/icons/svg/2286/2286182.svg");
    const [autor, setAutor] = useState({});

    useEffect(() => {
        Controller.getUsuarioById(props.receta.id_perfil)
            .then(data => setAutor(data))
            .catch(err => console.log(err))
    }, [])

    const toggleYummy = () => {
        if (yummy === "https://www.flaticon.com/svg/static/icons/svg/1933/1933811.svg") {
            setYummy("https://www.flaticon.com/svg/static/icons/svg/1933/1933790.svg")
            // + 1 like
            //setYummyBeat(1);
        }
        else setYummy("https://www.flaticon.com/svg/static/icons/svg/1933/1933811.svg");
    }

    const toggleFavMenu = () => {
        (favMenu === "https://www.flaticon.com/svg/static/icons/svg/2286/2286182.svg") ?
            setFavMenu("https://www.flaticon.com/svg/static/icons/svg/2286/2286153.svg") : setFavMenu("https://www.flaticon.com/svg/static/icons/svg/2286/2286182.svg");
    }

    return (
        <>
            <Card style={{ marginTop: "70px" }}>
                <CardHeader style={{ padding: "20px" }}>
                    <Row>

                        <div style={{ position: "relative" }}>
                            <ImgPerfil src="https://scontent-cdt1-1.cdninstagram.com/v/t51.2885-19/s150x150/12269951_1496220500684125_1811627870_a.jpg?_nc_ht=scontent-cdt1-1.cdninstagram.com&_nc_ohc=vJpBKE8Kg7MAX_B7Q1I&tp=1&oh=e7f6ef0ec1903d613cebd672926f20b4&oe=5FE90801" alt="Foto de perfil" />
                        </div>

                        <Col>
                            <Link to={"/" + autor._id} style={{ color: "black", textAlign: "left", marginLeft: "60px" }}>{autor.nombre}</Link>
                        </Col>
                    </Row>
                </CardHeader>
                <CardImg top width="100%" src="https://img.vixdata.io/pd/jpg-large/es/sites/default/files/imj/elgranchef/R/Receta-de-paella-1.jpg" alt="Card image cap" />
                <CardBody>
                    <Row style={{ display: "flex", alignItems: "center", padding: "0px 15px 10px 15px" }}>
                        <IconsRecetas onClick={() => toggleYummy()} src={yummy} alt="Yummy Icon" />
                        <IconsRecetas src="https://www.flaticon.com/svg/static/icons/svg/107/107784.svg" alt="Share Icon" />
                        <img style={{ height: 30, marginRight: 5 }} src="https://www.flaticon.com/svg/static/icons/svg/851/851011.svg" alt="Share Icon" />
                        <span>{props.receta.tiempo} min.</span>
                        <div style={{ flexGrow: 1, display: "flex", justifyContent: "flex-end" }}>
                            <IconsRecetas onClick={toggleFavMenu} src={favMenu} alt="Save Menu Icon" />
                        </div>
                    </Row>
                    <CardTitle tag="h5">{props.receta.nombre}</CardTitle>
                    <CardText>{props.receta.categoria}</CardText>
                    <Button block>Receta Completa</Button>
                </CardBody>
                <CardFooter style={{ display: "flex", flexWrap: "nowrap", alignItems: "center" }}>
                    <Input placeholder="Añade un comentario a esta receta..."></Input>
                    <Button color="link">Comentar</Button>
                </CardFooter>
            </Card>

            <div>Icons made by <a href="https://www.flaticon.com/authors/pixel-perfect" title="Pixel perfect">Pixel perfect</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
            <div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

        </>
    )
}

export default CardReceta;