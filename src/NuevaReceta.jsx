import React, { useState } from "react";
import {
    Container, Row, Col,
    FormGroup, Label, Input, FormText,
    Table,
    Button
} from 'reactstrap';
import { Redirect } from "react-router-dom";

import NuevoIngre from './NuevoIngre';
import NuevoPaso from './NuevoPaso';
import EditIngre from './EditIngre';
import EditStep from './EditStep';
import Controller from './AppController'


const NuevaReceta = (props) => {

    /* STATES CAMPOS FORMULARIO */
    const [nombre, setNombre] = useState('');
    const fecha = new Date();
    const [foto, setFoto] = useState();
    const [categoria, setCategoria] = useState('');
    const [tiempo, setTiempo] = useState('');
    const userId = props.id;
    const [ingredientes, setIngredientes] = useState([]);
    const [pasos, setPasos] = useState([]);
    const [volver, setVolver] = useState(false)

    /* SI NO ES USUARIO REGISTRADO REDIREGE A LA HOME */
    const logged = props.log;

    if (!logged) return <Redirect to="/"/>
    if(volver) return <Redirect to="/:user" />

    /* TABLAS NO VISIBLES SI NO HAY ELEMENTOS */
    let showIngres = "inherit"
    let showSteps = "inherit"

    if (ingredientes.length === 0) showIngres = "none"
    if (pasos.length === 0) showSteps = "none"

    /* ELIMINAR FILA DE UNA TABLA */
    const deleteLine = (idx, arr, setarr) => {
        let x = [...arr];
        x.splice(idx, 1);
        setarr(x);
    }


    /* CREACION DE FILAS DE LA TABLA INGREDIENTES */
    const listadoIngres = ingredientes.map((el, idx) => {

        if (el.cantidad && el.unidad) el.pron = "de";

        return (
            <tr key={idx}>
                <td>{el.cantidad} {el.unidad} {el.pron} {el.nombre}</td>
                <td><EditIngre idx={idx} array={ingredientes} setarray={setIngredientes} /></td>
                <td><Button onClick={() => deleteLine(idx, ingredientes, setIngredientes)} className="btn btn-danger btn-sm" >ELIMINAR</Button></td>
            </tr>
        )
    });

    /* CREACION DE FILAS DE LA TABLA PASOS */
    const listadoPasos = pasos.map((el, idx) => {

        return (
            <tr>
                <td>{idx + 1}</td>
                <td>{el.paso}</td>
                <td><EditStep idx={idx} array={pasos} setarray={setPasos} /></td>
                <td><Button onClick={() => deleteLine(idx, pasos, setPasos)} className="btn btn-danger btn-sm" >ELIMINAR</Button></td>
            </tr>
        )
    });


    /* SUBMIT FINAL - ENVIO DE DATOS A LA API */
    const guardar = () => {

        const recetaNueva = {
            nombre,
            fecha,
            foto,
            tiempo,
            categoria,
            id_perfil: userId,
            ingredientes,
            pasos
        };

        Controller.addReceta(recetaNueva, userId);
    }

    return (
        <>
            <Container className="mt-5">
                <h3>Añadir receta</h3>
                <hr />
                <br />
                <FormGroup>
                    <Label for="nombre">NOMBRE RECETA</Label>
                    <Input type="text" name="nombre" id="nombre" value={nombre} onChange={(e) => setNombre(e.target.value)} />
                </FormGroup>
                <br />
                <FormGroup>
                    <Label for="foto" >IMAGEN</Label>
                    <Input type="file" name="foto" id="foto" value={foto} onChange={(e) => setFoto(e.target.value)} />
                    <FormText color="muted">
                        Sube la mejor imagen de tu receta e impresiona a tus seguidores!
                    </FormText>
                </FormGroup>
                <br />
                <FormGroup>
                    <Label for="categoria">CATEGORIA</Label>
                    <Input type="select" name="categoria" id="categoria" value={categoria} onChange={(e) => setCategoria(e.target.value)}>
                        <option>---</option>
                        <option>Desayunos</option>
                        <option>Entrantes</option>
                        <option>Vegetales</option>
                        <option>Carnes</option>
                        <option>Pescados</option>
                        <option>Legumbres</option>
                        <option>Pasta / Arroz</option>
                        <option>Postres</option>
                    </Input>
                </FormGroup>
                <br />
                <FormGroup>
                    <Label for="tiempo">TIEMPO DE EJECUCIÓN</Label>
                    <Input type="number" name="tiempo" id="tiempo" value={tiempo} onChange={(e) => setTiempo(e.target.value)} />
                </FormGroup>

                <br />
                <p>INGREDIENTES</p>
                <Row>
                    <Col md="4">
                        <NuevoIngre array={ingredientes} setarray={setIngredientes} />
                    </Col>
                    <Col md="8">
                        <Table striped style={{ display: showIngres }}>
                            <tbody>
                                {listadoIngres}
                            </tbody>
                        </Table>
                    </Col>
                </Row>

                <br />
                <p>PASOS</p>
                <NuevoPaso setarray={setPasos} array={pasos} />
                <br />
                <Table striped style={{ display: showSteps }}>
                    <tbody>
                        {listadoPasos}
                    </tbody>
                </Table>

                <br />
                <Row >
                    <Col className="d-flex justify-content-end">
                    <Button color="success" className="btn-lg mb-5" onClick={guardar} >Guardar</Button>
                    </Col>
                    <Col>
                    <Button color="secondary" className="btn-lg mb-5" onClick={() => setVolver(true)} >Volver</Button>
                    </Col>
                </Row>
            </Container>
        </>
    );
};

export default NuevaReceta;