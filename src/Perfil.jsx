import { useEffect, useState } from 'react';
import Controller from './AppController';
import { Link, Redirect } from 'react-router-dom';
import {
    Card, Button, CardHeader, CardFooter, CardBody,
    CardTitle, CardText, CardImg, Row, Col
} from 'reactstrap';

const Perfil = (props) => {
    const [recetas, setRecetas] = useState([]);
    const [open, setOpen] = useState(false);
    const [selected, setSelected] = useState({});
    const username = props.user;

    const openReceta = (e) => {
        setOpen(true);
        setSelected(e);
    }

    const volver = () => {
        setOpen(false);
    }

    useEffect(() => {
        const userId = props.id;


        Controller.getAllRecetas()
            .then(recet => {
                const receta = recet.filter(el => el.id_perfil === userId);
                setRecetas(receta);
            })
            .catch(err => {
                console.log("Error en almacenar la info en los states: " + err)
                // return <Redirect to="/..." />;
            });
    }, [])

    if (!props.log) return <Redirect to="/" />

    return (
        <div>
            <h1 className="m-5">ESTO ES EL PERFIL DE {props.user.toUpperCase()}</h1>
            <Link to="/nuevo" className="btn btn-info m-5">CREAR RECETA</Link>
            <br />
            {open &&
                <>
                    <Button onClick={volver}>Volver</Button>
                    <Card>
                        <CardHeader>
                            <Row>
                                <Col>
                                    <a style={{ textAlign: "left", marginLeft: "50px" }}>{username}</a>
                                </Col>
                            </Row>
                        </CardHeader>
                        <CardImg top width="100%" src={selected.foto} alt={selected.nombre} />
                        <CardBody>
                            <Row style={{ padding: "0px 15px 10px 15px" }}>
                                <span>{selected.tiempo}</span>
                            </Row>
                            <CardTitle tag="h5">{selected.nombre}</CardTitle>
                        </CardBody>
                    </Card>
                </>
            }
            {!open &&

                recetas.map((e, idx) => (
                    <div style={{ display: 'inline-flex', flexWrap: 'wrap', alignItem: 'stretch' }} key={idx}>
                        <img style={{ display: 'flex', width: '250px', height: '250px', margin: '2px', objectFit: 'cover' }} src={e.foto} alt={e.nombre} onClick={() => openReceta(e)} />
                    </div>
                ))
            }

        </div>
    );
}

export default Perfil;