import { useEffect, useState } from 'react';
import { BrowserRouter, Switch, Route, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Row, Col, Button, Container } from 'reactstrap';
import './App.css';

import Controller from './AppController';
import Home from './Home';
import Loggin from './Login';
import Registro from './Registro';
import Perfil from './Perfil';
import NuevaReceta from "./NuevaReceta";
import Receta from './Receta';
import NotFound from './NotFound';

function App() {

  const [idUser, setIdUser] = useState('');
  const [nameUser, setNameUser] = useState('');
  const [loggeado, setLoggeado] = useState(false);
  const [mensaje, setMensaje] = useState('')
  const [show, setShow] = useState('none')

  //OBTENER ID LOCALSTORAGE (SI HAY ALGUIEN REGISTRADO)
  useEffect(() => {
    let localUser = Controller.getLocalUserId()
    if(localUser) {
      setIdUser(localUser.id)
      setNameUser(localUser.nombre)
      setLoggeado(true)
    }
  },[])

  //BOTON PERFIL (display & mensaje)
  useEffect(() => {

    if(loggeado){
      setShow("inherit");
      setMensaje("PERFIL DE " + nameUser.toUpperCase());
    } 
  }, [loggeado, nameUser])

  //LOGOUT
  const logout = () => {
    Controller.deleteUserId(nameUser);
    setLoggeado(false);
    setShow("none");
    setMensaje('');
  }

  return (
    <div className="App">
      <BrowserRouter>
        <Container>
          <Row className="mt-5">
            <Col md="6" className="d-flex justify-content-start align-items-center">
              <Link to="/" className="btn btn-success mx-5">HOME</Link>
              <Link to={"/"+nameUser} className="btn btn-success" style={{ display: show }}>{mensaje}</Link>
            </Col>
            <Col md="6" className="d-flex justify-content-end align-items-center">
              <Registro /> {''}
              <Loggin setlog={setLoggeado} setid={setIdUser} setuser={setNameUser} />
              <Button color="secondary" onClick={logout} className="btn-sm m-2">CERRAR SESIÓN</Button>
            </Col>
          </Row>

          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/nuevo" render={() => <NuevaReceta id={idUser} user={nameUser} log={loggeado} />} />
            <Route path="/receta/:id" component={Receta} />
            <Route exact path="/:user" render={() => <Perfil id={idUser} user={nameUser} log={loggeado} />} />
            <Route component={NotFound} />
          </Switch>
        </Container>
      </BrowserRouter>
    </div>
  );
}

export default App;
