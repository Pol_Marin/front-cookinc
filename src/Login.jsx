import React, { useEffect, useState } from 'react';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Form, FormGroup, Label, Input } from 'reactstrap';

import Controller from './AppController'

const Login = (props) => {

    /* STATES USER INFO (ELIMINAR Y PASAR DIRECTAMENTE POR PROPS???????) */
    const [userId, setUserId] = useState('');
    const [userName, setUserName] = useState('');
    const [nombre, setNombre] = useState('');
    const [password, setPassword] = useState('');

    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);

    const usuario = {
        nombre,
        password
    }

    useEffect(() => {

        const user = {
            id: userId,
            nombre: userName
        }

        if(userId){
            Controller.saveUserLocal(user);
            props.setlog(true);
            props.setid(userId);
            props.setuser(userName);
            setModal(!modal);
        }

    },[userId, userName])


    const buscaUser = () => {
        Controller.searchUser(usuario)
        .then(user => {
            setUserId(user[0]._id)
            setUserName(user[0].nombre)
        })
        .catch(err =>console.log(err));

        setModal(!modal);
    }

    return (
        <div>
            <div>
                <Button color="primary" onClick={toggle} className="m-2 btn-sm">INICIAR SESIÓN</Button>
                <Modal isOpen={modal} toggle={toggle}>
                    <ModalHeader toggle={toggle}>Identificate</ModalHeader>
                    <ModalBody>
                        <Form>

                            <FormGroup >
                                <Label for="nombre" >Nombre</Label>
                                <Input type="nombre" name="nombre" id="nombre" value={nombre} onChange={(e) => setNombre(e.target.value)}/>
                            </FormGroup>

                            <FormGroup >
                                <Label for="password" >Password</Label>
                                <Input type="password" name="password" id="password"  value={password} onChange={(e) => setPassword(e.target.value)}/>
                            </FormGroup>

                        </Form>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="info" onClick={buscaUser}>OK</Button>{' '}
                        <Button color="secondary" onClick={toggle}>CANCELAR</Button>
                    </ModalFooter>
                </Modal>
            </div>
        </div>
    );
}

export default Login;