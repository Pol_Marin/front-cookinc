import React, { useState, useEffect } from "react";
import { Container, Row, Col } from 'reactstrap';
import blanco from './img/blanco.svg';
import rojo from './img/rojo.svg';

import Controller from './AppController'

const Receta = (props) => {

    const [receta, setReceta] = useState({});
    const [yummies, setYummies] = useState();
    const [ingredientes, setIngredientes] = useState([{}]);
    const [pasos, setPasos] = useState([{}]);
    const [comentarios, setComentarios] = useState([{}]);
    const [like, setLike] = useState(false);

    useEffect(() => {
        const recetaId = props.match.params.id;
        Controller.getRecebtaById(recetaId)
            .then(data => {
                setReceta(data)
                setYummies(data.yummies)
                setIngredientes(data.ingredientes)
                setPasos(data.pasos)
                setComentarios(data.comentarios)
            })
            .catch(err => {
                console.log("Error en almacenar la info en los states: " + err)
                // return <Redirect to="/..." />;
            });
    }, [])

    const meGusta = () => {
        setLike(!like);
    }

    useEffect(() => {
        like ? setYummies(yummies + 1) : setYummies(0);
    }, [like])

    return (
        <>
            <Container>
                <Row className="centrado m-5">
                    <Col xs={6} sm={8} md={10} lg={12}>
                        <h1>{receta.nombre}</h1>
                    </Col>
                </Row>
                <Row className="centrado">
                    <Col xs={6} sm={8} md={10} lg={12}>
                        <div className="divfoto">
                            <img className="foto" src="http://placekitten.com/700/400" alt={receta.nombre} />
                        </div>
                    </Col>
                </Row>
                <Row className="m-5">
                    <Col xs={6} sm={8} lg={6}>
                        <div className="centrado">
                            <h5>{receta.tiempo + " minutos"}</h5>
                        </div>
                    </Col>
                    <Col xs={6} sm={8} lg={6}>
                        <div className="centrado">
                            <img className="icon" src={like ? rojo : blanco} onClick={meGusta} alt="yummie"></img>
                            <h5 className="yummies">{yummies}</h5>
                        </div>
                    </Col>
                </Row>
                <Row className="centrado">
                    <Col xs={6} sm={8} md={10} lg={12}>
                        <div className="centrado">
                            <h4>{"Ingredientes"}</h4>
                        </div>
                    </Col>
                </Row>
                <Row className="centrado">
                    <Col xs={6} sm={8} md={10} lg={12}>
                        <div className="centrado">
                            {ingredientes.map((e, idx) => (
                                <div key={idx}>
                                    <p>{e.cantidad} {e.unidad} {e.nombre}</p>
                                </div>
                            ))}
                        </div>
                    </Col>
                </Row>
                <Row>
                    <br />
                </Row>
                <Row className="centrado">
                    <Col xs={6} sm={8} md={10} lg={12}>
                        <div className="centrado">
                            <h4>{"Pasos"}</h4>
                        </div>
                    </Col>
                </Row>
                <Row className="centrado">
                    <Col xs={6} sm={8} md={10} lg={12}>
                        <div className="centrado">
                            {pasos.map((e, idx) => (
                                <div key={idx}>
                                    <br />
                                    <p>{idx + 1}- {e.paso}</p>
                                </div>
                            ))}
                        </div>
                    </Col>
                </Row>
                <br />
                <Row className="centrado">
                    <Col xs={6} sm={8} md={10} lg={12}>
                        <div className="centrado">
                            <h4>{"Comentarios"}</h4>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <div>
                        {comentarios.map((e, idx) => (
                            <Col key={idx} xs={12} sm={8} md={12} lg={12}>
                                <div className="comment">
                                    <br />
                                    <p>{e.comentario}</p>
                                </div>
                            </Col>
                        ))}
                    </div>
                </Row>
            </Container>
        </>
    );
};

export default Receta;